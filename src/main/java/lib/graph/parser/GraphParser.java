package lib.graph.parser;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class GraphParser {

    public static Graph parse(Path path) {

        BufferedReader br = null;
        List<String> lines = new ArrayList<String>();

        try {
            br = new BufferedReader(new FileReader(path.toString()));

            String line = br.readLine();

            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Graph<String, DefaultEdge> directedGraph = new DefaultDirectedGraph<>(DefaultEdge.class);

        // Walk through all lines
        for (String line : lines) {
            if (line.startsWith("knoten ")) {
                // Create all vertexes
                String vertexName = line.split(" ")[1];
                System.out.println("Knoten gefunden: " + vertexName);
                directedGraph.addVertex(vertexName);
            } else if (line.startsWith("kante ")) {
                // add all edges
                String[] split = line.split(" ");
                String edgeName11 = split[1];
                String edgeName12 = split[2];
                System.out.println("Kante gefunden: " + edgeName11 + " -> " + edgeName12);
                directedGraph.addEdge(edgeName11, edgeName12);
            }
        }

        return directedGraph;
    }

}
