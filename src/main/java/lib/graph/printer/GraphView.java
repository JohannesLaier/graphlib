package lib.graph.printer;

import javax.swing.*;
import com.mxgraph.layout.*;
import com.mxgraph.swing.*;
import org.jgrapht.*;
import org.jgrapht.ext.*;
import org.jgrapht.graph.*;
import java.awt.*;

public class GraphView extends JApplet {

    private static final long serialVersionUID = 2202072534703043194L;

    private Dimension size;

    private JGraphXAdapter<String, DefaultEdge> jgxAdapter;

    private Graph graph;

    public GraphView(Graph graph, Dimension size) {
        this.size = (size == null) ? new Dimension(530, 320) : size;
        this.graph = graph;
        this.init();
    }

    public javax.swing.JFrame display() {
        JFrame frame = new JFrame();
        frame.getContentPane().add(this);
        frame.setTitle("Graph");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        return frame;
    }

    @Override
    public void init() {
        // create a visualization using JGraph, via an adapter
        jgxAdapter = new JGraphXAdapter<>(graph);

        setPreferredSize(size);
        mxGraphComponent component = new mxGraphComponent(jgxAdapter);
        component.setConnectable(false);
        component.getGraph().setAllowDanglingEdges(false);
        getContentPane().add(component);
        resize(size);

        // positioning via jgraphx layouts
        mxCircleLayout layout = new mxCircleLayout(jgxAdapter);

        // center the circle
        int radius = 100;
        layout.setX0((size.width / 2.0) - radius);
        layout.setY0((size.height / 2.0) - radius);
        layout.setRadius(radius);
        layout.setMoveCircle(true);

        layout.execute(jgxAdapter.getDefaultParent());
        // that's all there is to it!...
    }
}
