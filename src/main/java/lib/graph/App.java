package lib.graph;

import lib.graph.parser.GraphParser;
import lib.graph.printer.GraphView;
import org.jgrapht.Graph;

import java.awt.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class App {
    public static void main(String[] args) {
        Path path = FileSystems.getDefault().getPath("data/Euler2.txt").toAbsolutePath();

        Graph graph = GraphParser.parse(path);
        GraphView view = new GraphView(graph,new Dimension(1920, 1080));

        view.display();
    }
}
